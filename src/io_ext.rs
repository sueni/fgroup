use std::ffi::OsStr;
use std::io::{self, BufRead};
use std::os::unix::ffi::OsStrExt;
use std::path::PathBuf;

pub trait BufReadExt: io::BufRead {
    fn path_lines(self) -> Paths<Self>
    where
        Self: Sized,
    {
        Paths { buf: self }
    }
}

impl<B: io::BufRead> BufReadExt for B {}

#[derive(Debug)]
pub struct Paths<B> {
    buf: B,
}

impl<B: BufRead> Iterator for Paths<B> {
    type Item = io::Result<PathBuf>;

    fn next(&mut self) -> Option<io::Result<PathBuf>> {
        let mut buf = Vec::new();
        match self.buf.read_until(b'\n', &mut buf) {
            Err(e) => Some(Err(e)),
            Ok(0) => None,
            Ok(_) => Some(Ok(PathBuf::from(OsStr::from_bytes(&buf[..buf.len() - 1])))),
        }
    }
}
