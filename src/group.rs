use std::error::Error;
use std::path::{Path, PathBuf};
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Debug)]
pub struct Group {
    pub clan: PathBuf,
    pub most_recent: SystemTime,
    pub paths: Vec<PathBuf>,
}

fn mtime(item: &Path) -> Result<SystemTime, Box<dyn Error>> {
    Ok(item.metadata()?.modified()?)
}

impl Group {
    pub fn new(clan: PathBuf) -> Self {
        Self {
            clan,
            most_recent: UNIX_EPOCH,
            paths: Vec::new(),
        }
    }

    pub fn push(&mut self, item: PathBuf) {
        if let Ok(mtime) = mtime(&item) {
            if self.most_recent < mtime {
                self.most_recent = mtime;
            }
            self.paths.push(item);
        }
    }

    pub fn sort(&mut self) {
        self.paths
            .sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&a, &b));
    }

    pub fn reverse(&mut self) {
        self.paths.reverse();
    }
}
