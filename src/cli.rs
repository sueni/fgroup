use std::error::Error;
use std::ffi::OsString;
use std::fmt;
use std::path::PathBuf;

pub const HELP: &str = "Usage: <FILES> | [FGROUP=reverse] fgroup <DIRECTORY>";

#[derive(Debug)]
pub enum AppError {
    OrderError,
    DirectoryError,
}

impl Error for AppError {}
impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AppError::OrderError => write!(f, "Invalid order"),
            AppError::DirectoryError => write!(f, "Invalid directory"),
        }
    }
}

pub enum Order {
    Reverse,
    Default,
}

impl TryFrom<Option<OsString>> for Order {
    type Error = AppError;

    fn try_from(value: Option<OsString>) -> Result<Self, Self::Error> {
        match value {
            Some(mode) if mode == *"reverse" => Ok(Self::Reverse),
            Some(_) => Err(AppError::OrderError),
            None => Ok(Self::Default),
        }
    }
}

pub struct Args {
    pub order: Order,
    pub entry_point: PathBuf,
}

pub fn parse_args() -> Result<Args, AppError> {
    let order: Order = std::env::var_os("FGROUP").try_into()?;

    let entry_point = match std::env::args_os().nth(1) {
        Some(arg) => PathBuf::from(arg),
        None => return Err(AppError::DirectoryError),
    };

    if !entry_point.is_dir() {
        return Err(AppError::DirectoryError);
    }

    Ok(Args { order, entry_point })
}
