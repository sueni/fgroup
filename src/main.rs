use std::io::{self, BufReader, BufWriter, Write};
use std::os::unix::ffi::OsStrExt;

mod cli;
mod group;
mod io_ext;

use cli::Order;
use group::Group;
use io_ext::BufReadExt;

fn sort(groups: &mut [Group], order: Order) {
    groups.iter_mut().for_each(|group| group.sort());
    match order {
        Order::Reverse => {
            if groups.len() == 1 {
                groups[0].reverse();
            } else {
                groups.sort_unstable_by(|a, b| a.most_recent.cmp(&b.most_recent));
            }
        }
        Order::Default => groups.sort_unstable_by(|a, b| b.most_recent.cmp(&a.most_recent)),
    }
}

fn display(groups: &[Group]) -> io::Result<()> {
    let mut writer = BufWriter::with_capacity(64 * 1024, io::stdout());
    for group in groups {
        for p in &group.paths {
            writer.write_all(p.as_os_str().as_bytes())?;
            writer.write_all(b"\n")?;
        }
    }
    Ok(())
}

fn main() {
    let cli::Args { order, entry_point } = match cli::parse_args() {
        Ok(args) => args,
        Err(err) => {
            eprintln!("ERROR: {}\n{}", err, cli::HELP);
            std::process::exit(1);
        }
    };

    let mut groups: Vec<Group> = Vec::with_capacity(64);
    let files = BufReader::new(io::stdin())
        .path_lines()
        .flatten()
        .filter(|p| p.is_file());

    for file in files {
        match file.parent() {
            Some(parent) if parent == entry_point => {
                let mut group = Group::new(parent.to_path_buf());
                group.push(file);
                groups.push(group);
            }
            Some(parent) => {
                if let Some(clan) = parent
                    .ancestors()
                    .find(|a| a.parent() == Some(&entry_point))
                {
                    match groups.iter_mut().rev().find(|g| g.clan == clan) {
                        Some(same_group) => same_group.push(file),
                        None => {
                            let mut group = Group::new(clan.to_path_buf());
                            group.push(file);
                            groups.push(group);
                        }
                    }
                }
            }
            None => unreachable!(),
        }
    }
    sort(&mut groups, order);
    display(&groups).unwrap();
}
